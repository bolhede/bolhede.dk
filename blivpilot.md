---
layout: default
title: Bliv Pilot
---

Bliv Pilot
===========
 
### Uddannelsen

Uddannelsen til svæveflyvepilot er ikke vanskelig, men den er grundig. Sammen med en af klubbens certificerede instruktører, flyver eleven i et af klubbens tosædede skolefly, hvor eleven lærer at flyve. Dette gøres typisk på de første 70-80 starter, hvorefter eleven får lov til at flyve solo og bliver så omskolet til et èt-sædet fly mens skolingen fortsætter.

![Cumulus](/assets/img/skoling.jpg)

Om venteren er der teorikursus som varer ca. 50 undervisningstimer, hvilket svare til ca. én formiddag om ugen i vinterperioden.

Der kræves intet forudgående kendskab til flyvning, da meget at teorien læres under flyvningen med instruktøren.

Kurset afsluttes med en teoriprøve der typisk ligger i april, hvorefter skoleflyvningen fortsætter. 

### 1\. FAST TRACK

Du starter din pilot uddannelse men en flyvetur i klubbens motordrevne TMG,

![Cumulus](/assets/img/falken.jpg)

hvor du lære de mest basale ting i flyvningen. En instruktør vil guide dig igennem elementer som drej og fartstyring. Efter nogle få ture vil du have forudsætningerne for at komme godt i gang med skolingens næste fase.

### 2\. GRUNDSKOLING

I grundskolingen deltager du sammen med andre pilotaspiranter i de grundlæggende aspekter i flyvningen. Dit fokus vi på dette tidspunkt af din uddannelse centrerer sig om flyvningens start- og landingsfaser. Når du efter en periode på typisk mellem 2-3 mdr. mestre svæveflyvningens grundelementer er du klar til næste del af forløbet.

![Cumulus](/assets/img/skolinglanding3.jpg)

### 3\. SOLO FLYVNING

Denne del af uddannelsen er enhver pilots største øjeblik. Det er her du for første gang vil flyve uden af have en instruktør med i flyveren. Lige meget hvilken type pilot du er, vil du altid huske din første soleflyvning til evig tid.

![Cumulus](/assets/img/soloflyvning.jpg)

### 4\. VIDREGÅENDE SKOLING

Efter Soloflyvningen vil der i din uddannelse blive lagt yderligere lag på de færdigheder du erhvervede dig under din grundlæggende skoling. Under din videregående skoling vil der derfor blive stillet højere krav til præcision, i forhold til hvad der gjorde i tidligere faser af uddannelsen. Du gennemgår dette forløb i en kombination af Soloflyvninger og med flyvninger med en tilknyttet instruktør.

### 5\. FLYVEPRØVEN OG CERTIFIKAT

Denne fase er den sidste og afsluttende del af din pilotuddannelse. Du vil her i lighed med en traditionel praktisk køreprøve skulle demonstrere, at du til enhver tid og i pressede situationer kan fører tilfredsstillende og sikkert.



__Foruden det praktiske forløb i uddannelsen, modtager du et teoretisk kursus hen over vinteren, som hjælper dig med at forstå teorien bag flyvning bedre. Denne teori afsluttes med en prøve i foråret. Bare rolig – Det er ikke så kedeligt som det måske kan lyde.__