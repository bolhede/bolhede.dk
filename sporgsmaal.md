---
layout: default
title: Om svæveflyvning
---

# Hvad er svæveflyvning?

Svæveflyvning er flyvning med et konventionelt fly, uden brug af moter. Dette kan lade sig gøre ved, at luft passerer henover vingen, når flyet bevæger sig fremad, men da et svævefly ikke bruger nogen mortor, skal det også bevæge sig en smule nedad for at holde farten. Et moderne svævefly fra Vestjysk Svæveflyveklub kan flyve 48 meter vandret, for hver meter det synker. Det betyder , at det fra 500 m. højde kan flyve 24 km. fremad! Svæveflyet kan dog på nogle dage også flyve meget længere. Dette kan lade sig gøre, hvis piloten finder nogle opadgående luftstrømme - termik - som er kraftige nok til at løfte flyet op i stor højde - nogle dage op over 2000 m.!

![Cumulus](/assets/img/cumulus.jpg)
Termik kan ofte genkendes på de hvide Cumulus skyer, også kaldet "blomkåls skyer"

# Hvem kan blive pilot?

Svæveflyvning er for begge køn i næsten alle aldersgrupper. I Vestjysk Svæveflyveklub er medlemmerne i allersgruppen fra 14-75 år.
Man kan altså starte på flyvningen som 14 årig, mens minimums-alderen for at flyve alene (solo) er 15 år og for at få udstedt sit S-certifikat, skal man være flydt 16 år.

Alle med et normalt helbred kan få lov til at flyve. F.eks. er brug af briller normalt ingen hindring. Det er dog et krav at man skal gennemgå en lægeundersøgelse før man får lov til at flyve solo.

# Er det farligt?

Svæveflyvere er ikke vovehalse. Vi er almindelige mennesker, der gør drømmen om at flyve til virkelighed, uden at løbe en unødig risiko.
Grundlæggende er svævefly simple og robuste konstruktioner, uden motor eller anden avanceret mekanik og tekniske fejl er yderst sjældne.

Den største risiko sider bag styrepinden. Det er derfor vi gør meget ud af både den praktiske og teoretiske del af svæveflyveuddannelsen.