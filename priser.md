---
layout: default
title: Priser
---

Priser
======

### MEDLEM

#### kr. 700 per månede

* UBEGRÆNSET SVÆVEFLYVNING
* UBEGRÆNSET SPILSTATER
* FRI ADGANG TIL KLUBBENS SVÆVEFLY
* ADGANG TIL KLUBBENS TMG
* TILKNYTTET INDSTRUKTØR
* MULGIHED FOR KLUBTRÆNER

### UNG MEDLEM

#### kr. 450 per månede

* UBEGRÆNSET SVÆVEFLYVNING
* UBEGRÆNSET SPILSTATER
* FRI ADGANG TIL KLUBBENS SVÆVEFLY
* ADGANG TIL KLUBBENS TMG
* TILKNYTTET INDSTRUKTØR
* MULGIHED FOR KLUBTRÆNER

### TMG MEDLEM

#### kr. 570 per månede

* ADGANG TIL KLUBBENS TMG
* 450 KR. PR. TACHO TIME
* TILKNYTTET INDSTRUKTØR

Ved indmeldelse opkræves du 4600 kr. i Indskud, som betales over 3 rater. I Indskuddet ligger 3 mdr. medlemskab, div opstartsomkostninger så som uddannelses materialer og obligatorisk medlemskab af [DSVU](http://dsvu.dk) (Dansk svæveflyver Union).