---
layout: default
title: Få en prøvetur
---

Få en prøvetur
=====

Kunne du tænke dig, at prøve kræfter med svæveflyvning, er det altid muligt komme forbi Vestjysk Svæveflyveklub, og købe en prøvetur med en af vores dygtige piloter. Vær forberedt på at ventetid kan forekomme, da klubbens elever som udgangspunkt har 1. prioritet.

En prøvetur koster kun 200 kr. og varer typisk 5-15 min. Da svævefly bliver båret af naturens kræfter, vil flyveturene kunne variere afhængig af vejrforholdene den pågældende dag.

Det vil også være muligt, at prøve klubbens motorsvævefly for samme pris.

Hos Vestjysk svæveflyveklub arrangerer vi på hverdage også større gruppeflyvninger for firmaer, skoler, institutioner og lignende. Som udgangspunk starter gruppeflyvninger ved min 10 flyvninger - tryk her og hør nærmere om mulighederne for gruppeflyvninger på hverdage.