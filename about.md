---
layout: default
title: Om klubben
---

Om klubben
=====

Hos Vestjysk svæveflyveklub kan du dyrke din passion for luftsport på alle niveauer. Hos Vestjysk svæveflyveklub tilgodeser vi både den seriøse konkurrencepilot og piloten, som blot ønsker lidt hyggeflyvning i de vante omgivelser.

At svæveflyvning er en sport for alle, ses tydeligt i sammensætningen af medlemmer i klubben. Hos Vestjysk svæveflyveklub har vi medlemmer i alle aldersgruppe lige fra 14 til 85 år, og der er ingen forskel på om man er mand eller kvinde.

Vestjysk svæveflyveklub skal være et godt sted at være, derfor er de enkelte medlemmers familier altid velkomne til at deltage i klubben arrangementer, som eksempelvis fester og klubturer

Hvor bor vi?
---

Vores flyveplads er beliggende ved Tingvejen cirka midt imellem Esbjerg og Grinsted, på højde med Agerbæk.

Ved Vrenderup plantage står der et skilt der viser mod svæveflyvepladsen ad Bolhedevej.

Et par kilometer nede ad Bolhedevej, står et skiltmed klubbens logo. Her køre du ad grusvejen og snart er du ved klubhuset og hangarerne.

#### Adressen er Bolhedevej 6, Bolhede, 6800 Varde
<iframe
  width="600"
  height="450"
  frameborder="0" style="border:0"
  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBB1AcP8dqOJ8xz7RVRiT3aOmHrqugZ01A&q=Bolhedevej%206,%20Varde,%20Danmark" allowfullscreen>
</iframe>


Hvornår flyves der?
---
I Vestjysk Svæveflyveklub flyves der hver weekend og på helligdage i perioden 1. april til 1. november.
Herudover arrangerer klubben hvert år en sommerlejr - typisk én uge i juli. I sommerlejren kører skolingen intensivt, altså hver dag.



